#! /usr/bin/python

# Filename: projectBuilder.py
# Builds the OpenMRS API module, and 
# skips all included unit tests.

import shutil 
import os


# compile the api module of openmrs
os.chdir("../project/src/openmrs/api/")
cmd = 'mvn clean install -DskipTests=true'  
os.system(cmd)
os.chdir("../../../../scripts/")


