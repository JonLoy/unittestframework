#! /usr/bin/python
# 
# Filename: reportMaker.py
# Generates testReportXX.html by reading each
# testCaseXXreports.txt file writing out the
# test case number, component tested, method tested,
# the requirement, test inputs, expected outcome, and outcome.
# 

import os
import webbrowser
import datetime

passedTests = 0
numberOfTests = 0
failedTests = []

def main(testStatus):
   """Generates the report file"""
   global numberOfTests

   # used to increment the testReportXX.html filename
   count = 1
   for file in os.listdir("../reports/"):    
      if file.endswith('.html'):
         count += 1

   reportName = "testReport" + str(count) + ".html"
   reportFile = open("../reports/" + reportName, 'w')
   beginHTML(reportFile, count)

   if testStatus > 0:
      tableHeadingsWriter(reportFile)
      # creates a list of text files in the testCases directory
      testResultsList = []
      os.chdir("../temp/")
      for file in os.listdir('.'):
        if file.endswith('.txt'):
         testResultsList.append(file)
         numberOfTests = numberOfTests + 1

      testResultsList.sort()

      for file in testResultsList:
         tableWriter(reportFile, file)

      reportFile.write("</table>")
   else:
      noTests(reportFile)

   endHTML(reportFile)

   # opens the testReportsXX.html file
   webbrowser.open("../reports/" + reportName)

def beginHTML(reportFile, count):
   """Writes the beginning portion of the report file"""

   time = datetime.datetime.now()

   reportFile.write("<HTML>")
   reportFile.write("<HEAD>")
   reportFile.write("<TITLE>Team 2 - Test Report #" +str(count) + "</TITLE>")
   reportFile.write("</HEAD>")
   reportFile.write("<BODY style=\"font-family:new century schoolbook; background-color:PowderBlue;\">")
   reportFile.write("<img src=\"../docs/images/omrs-header.png\"> <H1>Synchronized Chaos</H1><hr>")
   reportFile.write("<H2> Team 2 - Test Report #" +str(count) + "</H2>")
   reportFile.write("<tr></tr>")
   reportFile.write("Test ran on " + time.strftime("%m-%d-%Y") + " at " + time.strftime("%H:%M"))
   reportFile.write("<tr></tr>")
   

def tableHeadingsWriter(reportFile):
   """Creates the table and writes the table Headings"""

   reportFile.write("<table style='border-collapse: collapse; border: solid;' align='center' border='1' cellpadding='1' cellspacing='2' width='99%' bgcolor='white'>")
   reportFile.write("<tr>")
   reportFile.write("<th width=2%>#</th>")
   reportFile.write("<th width=11%>Component</th>")
   reportFile.write("<th width=17%>Method</th>")
   reportFile.write("<th width=36%>Requirement</th>")
   reportFile.write("<th width=11%>Test Inputs</th>")
   reportFile.write("<th width=11%>Recorded Outcome</th>")
   reportFile.write("<th width=11%>Expected Outcome</th>")
   reportFile.write("<th width=7%>Result</th>")
   reportFile.write("</tr>")
   
def tableWriter(reportFile, file):
   """Generates an html table
   
   Reads in each file, creates a key-value dictionary from the testCaseXXresults.txt file
   It then calls tableRowWriter and results checker function to fill in the rows in the table
   
   """
   
   KEYWORDS = "TestCaseNumber: Component: Method:  Requirement: TestInputs: Outcome: ExpectedOutcome: Result:"
   keywords_set = set(KEYWORDS.split())

   resultsFile = open(file, "r")

   # creates an empty dictionary for storing text        
   results_dict = {}
   for line in resultsFile:
      if keywords_set.intersection(line.split()):
         firstword = line.partition(' ')[0]
         restOfLine = line.partition(' ')[2]
         results_dict[firstword] = restOfLine.strip()

   tableRowWriter(reportFile, results_dict)
   

def tableRowWriter(reportFile, results_dict):
   """Writes out the information on each test"""
   global passedTests
   global failedTests

   reportFile.write("<tr>")
   reportFile.write("<td>" + results_dict["TestCaseNumber:"] + "</td>")
   reportFile.write("<td>" + results_dict["Component:"] + "</td>")
   reportFile.write("<td>" + results_dict["Method:"] + "</td>")
   reportFile.write("<td>" + results_dict["Requirement:"] + "</td>")
   reportFile.write("<td>" + results_dict["TestInputs:"] + "</td>")
   reportFile.write("<td>" + results_dict["Outcome:"] + "</td>")
   reportFile.write("<td>" + results_dict["ExpectedOutcome:"] + "</td>")
   if results_dict['Result:'] == 'Pass':
     testResult = "<img src=\"../docs/images/pass.png\">"
     passedTests = passedTests + 1
     """Pass--green check mark"""
   else:
     testResult = "<img src=\"../docs/images/redX.png\">"
     failedTests.append(results_dict["TestCaseNumber:"])
     """Fail--red X"""
   reportFile.write("<td>" + testResult + "</td>")
   reportFile.write("</tr>")
 
def noTests(reportFile):
    reportFile.write("<div style='text-align:center;'>")
    reportFile.write("<p>There are no test cases. ")
    reportFile.write("<p>Please add test cases and run the framework again.</div>")

def endHTML(reportFile):
   """Writes the closing HTML tags"""
   global passedTests
   global numberOfTests
   global failedTests

   reportFile.write("<div style='text-align:center;'>Results: " + str(passedTests) + " of " + str(numberOfTests) + " Tests passed")
   if len(failedTests) > 0:
      reportFile.write("<p>The following tests failed:<ul> ")
      for i in failedTests:
        reportFile.write("<li>Test Case Number: <a href=../temp/TestCase" + i + "results.txt>" + i + "</a></li>") 
      reportFile.write("</ul></div>")
   
   reportFile.write("<tr></tr>")
   reportFile.write("<p align='center'><a href='http://openmrs.org/'><img src='../docs/images/openmrs-logo-small.gif' alt='OpenMRS'></a>   <a href ='https://svn.cs.cofc.edu/repos/CSCI3622012/team2/'>Team 2 SVN</a>   <a href='http://stono.cs.cofc.edu/~bowring/''>CSCI 362</a></p>")
   reportFile.write("</BODY>")
   reportFile.write("</HTML>")    