#! /usr/bin/python
# 
# Filename: frameworkRunner.py
# Runs the testing framework.
# Invokes helper scripts as needed. 

import testMaker
import reportMaker
import resultsChecker;
import os


testCases_path = "../testCases/"
testExecutables_path = "../testCasesExecutables/" 
scripts_path = "../scripts/"
reports_path = "../reports/"
temp_path = "../temp/"

def run():
	"""Runs the testing framework"""

	
	os.system('python projectBuilder.py')
	# Clean up section - remove files from temp and testCasesExecutables

	#removes any java files in the testCases directory
	os.chdir(testExecutables_path)
	for file in os.listdir('.'):
		if file.endswith('.java'):
			os.remove(file)
		if file.endswith('.class'):
			os.remove(file)

	# removes any files in the temp directory
	os.chdir(temp_path)
	for file in os.listdir('.'):
		if file.endswith('.txt'):	
			os.remove(file)

	os.chdir(scripts_path)
	testMaker.run()
	print "Now creating the test files."
	os.chdir(scripts_path)
	print "Now compiling and executing the test case files."
	os.system('python testCasesCompiler.py')
	os.chdir(scripts_path)
	resultsChecker.main()
	os.chdir(scripts_path)
	print "Building and opening the test report webpage."
	reportMaker.main(1)