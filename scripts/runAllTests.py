#! /usr/bin/python
#
# Filename: runAllTests.py
# A script to begin running the testing framework.
# Checks to see if any test cases exist.
# If test cases do not exist, the script ends. 
# Otherwise it invokes frameworkRunner.py to handle 
# the running of the testing framework.

import os
import frameworkRunner
import reportMaker

path = "testCases/"

testCases = []

for file in os.listdir(path):
        if file.endswith('.txt'):
        	testCases.append(file)
os.chdir("scripts/")

if testCases == []:
	reportMaker.main(0)
else:	
	frameworkRunner.run()