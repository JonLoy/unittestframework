#! /usr/bin/python
# Filename: testCasesCompiler.py
# Compiles each of the TestCaseXX.java files

import os
import subprocess
import fnmatch

os.chdir("../testCasesExecutables/")
classFiles = '../project/src/openmrs/api/target/classes:../project/bin/simple-xml-2.6.7/jar/simple-xml-2.6.7.jar:../project/bin/commons-logging-1.1.1/commons-logging-1.1.1.jar:.'

#compiles the TestCase*.java files the tests
for file in os.listdir('.'):
	if fnmatch.fnmatch(file, 'TestCase*.java'):
		cmd = 'javac -cp ' + classFiles + ' ' + file
		os.system(cmd)
for file in os.listdir('.'):
	if fnmatch.fnmatch(file, 'TestCase*.java'):
		target = os.path.splitext(file)[0]
		cmd = 'java -cp ' + classFiles + ' ' + target
		os.system(cmd)
