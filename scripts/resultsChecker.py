#! /usr/bin/python
# 
# Filename: resultsChecker.py
#
# The script compares the outcome with the expected outcome
# found in its corresponding oracle testCaseXXoracle.txt
# and determines if the test was a succes or a failure.
# 

import os

def main():
   # creates a list of text files in the testCases directory
   testResultsList = []
   os.chdir("../temp/")
   for file in os.listdir('.'):
      if file.endswith('.txt'):
         testResultsList.append(file)
     
   testResultsList.sort()

   for file in testResultsList:
      resultChecker(file)

def resultChecker(file):
   """Determines if the test Passed or Failed"""
   KEYWORDS = "TestCaseNumber: Component: Method:  Requirement: TestInputs: Oracle: Outcome: "
   keywords_set = set(KEYWORDS.split())

   resultsFile = open(file, "r+")

   results_dict = {}
   for line in resultsFile:
      if keywords_set.intersection(line.split()):
         firstword = line.partition(' ')[0]
         restOfLine = line.partition(' ')[2]
         results_dict[firstword] = restOfLine.strip()

   try:
      with open("../oracles/" + results_dict['Oracle:'], "r") as oracleFile:
         # creates an empty dictionary for storing text        
         oracle_dict = {}
         KEYWORD = "ExpectedOutcome:"
         keyword_set = set(KEYWORD.split())


         for line in oracleFile:
            if keyword_set.intersection(line.split()):
               firstword = line.partition(' ')[0]
               restOfLine = line.partition(' ')[2]
               oracle_dict[firstword] = restOfLine.strip()
         
         resultsFile.write("\nExpectedOutcome: " + oracle_dict["ExpectedOutcome:"] + '\n')

         if oracle_dict['ExpectedOutcome:'] == results_dict['Outcome:']:
            testResult = "Pass"
         else:
            testResult = "Fail"

   except IOError:
         resultsFile.write("\nExpectedOutcome: " + "Oracle not found\n") 
         testResult = "Fail"     
   resultsFile.write("Result: " + testResult)   
