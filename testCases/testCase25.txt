TestCaseNumber: 25
Component: Schedule.java
Method: void setDescription(String descr)
Requirement:  The method sets the description of the schedule
TestInputs: Test setting the description     
Oracle: TestCase25oracle.txt
Import: org.openmrs.*;
Import: org.openmrs.scheduler.*;

Schedule s = new Schedule();
s.setDescription("Test setting the description");
String outcome = s.getDescription();
out.write("Outcome: " + outcome);

