TestCaseNumber: 15
Component: Form.java
Method: String toString()
Requirement: Should set the Attribute Name of the instantiated Field 
TestInputs: 4321, 47
Oracle: TestCase15oracle.txt
Import: org.openmrs.*;

Form form = new Form(4321);
FormField ff = new FormField(47);
form.addFormField(ff);
String outcome = form.toString();
out.write("Outcome: " + outcome);
