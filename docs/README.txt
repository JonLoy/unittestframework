Welcome to Team 2's README document

--Last updated: December 4th, 2012--
Team Members: Jonathan Loy, Thomas Raab, Jalisa Greene, Bob Fitz add your own names

Open Source Project: OpenMRS

Objective: To develop an automated unit testing framework to perform 

Required software: OpenMRS 1.9.2, Apache Commons-logging-1.1.1, Simple-xml-2.6.7, Maven, Java JDK 1.6, Python 2.7.3,

Clockwork framework setup
This process assumes Ubuntu and Subversion are installed, and the user has access to the subversion repo hosted by the College of Charleston.

1) Open terminal.
2) Type in ‘svn co https://svn.cs.cofc.edu/repos/CSCI3622012/team2 \<target folder>’
3) Ensure the OpenMRS package is located within the project/src/ directory. If not please use the link listed to download the OpenMRS package. Make sure to place the OpenMRS package in project/src/
4) Ensure the Simple-xml and the Apache Commons Logging packages are located within the project/bin/ directory. If the packages are not there, please use the provided links to download the software and place it into project/bin/.
5) If all the files have been correctly placed (check against the provided Framework structure), in the terminal navigate to the <target folder>.
6) Type in ‘./scripts/runAllTests.py’#
7) The framework should build the OpenMRS API module, create test case drivers, execute the tests, and display an HTML file detailing the tests and their results.

---------------------------------------------------------------------
Framework Directory Structure:
	SynchronizedChaos/
		/project
			/src
				/openmrs								
			/bin
				/simple-xml-2.6.7
					simple-xml-2.6.7.jar
				/commons-logging-1.1.1
					commons-logging-1.1.1.jar
		/scripts
			runAllTests.py
			frameworkRunner.py
			testCasesCompiler.py
			projectBuilder.py
			testMaker.py
			resultsChecker.py
			reportMaker.py
		/testCases
			testCaseXX.txt // includes test case 01 - 25
		/testCasesExecutables
			TestCaseXX.java // one java file for each test case
			TestCaseXX.class // one java file for each test case
		/temp
			TestCaseXXresults.txt // one for each test case
		/oracles
			TestCaseXXoracle.txt // one for each test case
		/docs
			/images
				pass.png
				redX.png
			/deliverables  -- historical deliverables
				TBD_deliverable1.pdf
				TBD_deliverable2.pdf
				TBD_deliverable3.pdf
				TBD_deliverable4.pdf
			README.txt
			testCaseTemplate.txt
			testCaseOracleTemplate.txt
			TestPlan.pdf
			SynchronizedChaos_deliverable5.pdf
		/reports
			testReportXX.html // incremented each time the framework is run.


Warnings
1)Be sure to place the OpenMRS platform within the SynchronizedChaos/project/src directory.
2)Place the required third party frameworks in the SynchronizedChaos/project/bin directory as specified in the framework structure above.

---------------------------------------------------------------------
Project:
src - Contains the source code necessary to run the project
bin - contains the Simple XML framework and Apache Commons Logging
---------------------------------------------------------------------

Scripts: 
runAllTests.py:
This script is invoked to begin running the testing framework. The script first checks to see if any test cases exist. If test cases do not exist, the framework is shut down, and informs the user to create test cases. Otherwise, the script calls frameworkRunner.py to take over the running of the testing framework.

frameworkRunner.py:
Runs the testing framework. Invokes helper scripts as needed to accomplish tasks.

testCasesCompiler.py:
Compiles each of the TestCaseXX.java files, then executes the TestCaseXX.class files.

projectBuilder.py:
Builds the OpenMRS API module, but skips all included unit tests.

testMaker.py:
Reads each testCaseXX.txt file in the testCases directory and creates the corresponding TestCaseXX.java and TestCaseXXresults.txt file with information from the test case text file.

resultsChecker.py:
Compares the record outcome of a test with the expected outcome found in its corresponding oracle to determine if the test passed or failed. The script also writes the expected outcome and test result to the individual result files.

reportMaker.py:
Generates an HTML file by reading each test case results file. The HTML file includes a table displaying the test case number, component tested, method tested, the requirement, test inputs, recorded outcome, expected outcome, and test result of each test case.
---------------------------------------------------------------------
TestCases:

Directory containing the specified test cases to run. See testCaseTemplate.txt to see test case template.

----------------------------------------------------------------------
TestCaseExecutables:

Directory containing the test cases to be compiled and run against the project software.

----------------------------------------------------------------------
Docs:
README.txt:
A standard readme file. Contains the directory structure, a short description of each directory and its’ contents, and details the project’s workflow.

TestPlan.pdf:
Defines the team’s testing process, the testing schedule, test cases, pass/fail criteria, test reporting and recording procedures, and the hardware and software requirements for building the complete OpenMRS platform.

testCaseOracleTemplate.txt:
A template for creating an oracle for a test case. The oracle must follow this template in order to be correctly used by the Clockwork framework. The oracle should be named testCaseXXoracle.txt, with the test case number replacing the XX.

testCaseTemplate.txt:
A template for creating a new test case. The test case must follow this template in order to be correctly used by the Clockwork framework. The test case and its code should be tested thoroughly before committing the testCaseXX.txt file to the repository. The test case should be number should replace the XX.

SynchronizedChaos_deliverable5.pdf:
This document. Provides all relevant information on the class project: introduction,  explanation  of  the  testing framework, detailed instructions on how to build test cases, oracles, and how the framework handles the automation of testing, the fault injection, etc.

SynchronizedChaos_deliverable1.pdf:
A team experience report. Includes a summary of chosen open source project, the process of checking out the project from a repository and building it, run existing tests and reporting results,  and evaluation of the experience and project.

SynchronizedChaos_deliverable2.pdf:
A detailed test plan containing at least 5 of the 25 required tests.

SynchronizedChaos_deliverable3.pdf:
Details the design and building of an automated unit testing framework to implement the specified test plan. Includes an architectural description of the framework, how-to documentation, a set of at least 5 of the 25 test cases.

SynchronizedChaos_deliverable4.pdf:
.Details the design and building of an automated unit testing framework to implement the specified test plan. Includes an architectural description of the framework, how-to documentation, all of the 25 test cases.

---------------------------------------------------------------------
Oracles:

Contains files with expected outcomes from the testCases

---------------------------------------------------------------------
Reports:

Contains reports for each individual unit test, and the general testing overview report.

testReport.html - A general testing overview report detailing which tests succeeded, which failed, and which weren't executed.

---------------------------------------------------------------------

Project Flow:

1) From the command line change directory to the parent directory of the testing framework.
2) From the parent directory type ‘./scripts/runAllTests.py’#
3) runAllTests.py checks the testCases directory to see if any testCaseXX.txt files exist. If test cases exist runAllTests.py calls frameworkRunner.run(), if no testCaseXX.txt files exists, the program will exit, and inform the user to supply testCaseXX.txt files.
4) frameworkRunner.py invokes projectBuilder.py which builds the API module of the OpenMRS platform.#
5) projectBuilder.py changes directory to project/src/openmrs/api and builds the OpenMRS API module via Maven.
6) frameworkRunner.py deletes all files from temp and testCasesExecutables directories.
7) frameworkRunner.py calls testMaker.run()
8) testMaker.run() reads in the testCaseXX.txt files then generates the corresponding TestCaseXX.java in the testCaseExecutables directory and TestCaseXXresults.txt files in the temp directory in accordance with their prescribed templates.
9) frameworkRunner.py then invokes testCasesCompiler.py
10) testCasesCompiler.py changes into the testCaseExecutables directory then compiles the TestCaseXX.java files to create the necessary TestCaseXX.class files.
11) testCasesCompiler.py then runs each TestCaseXX.class file which in turn write to their corresponding TestCaseXXresults.txt file.
12) Then frameworkRunner.py calls resultsChecker.py
13) First, resultsChecker.py locates the TestCaseXXresults.txt and testCaseXXoracle.txt files for each test case. Next, the script writes a new line to the results file with the information from the ‘ExpectedOutcome:’ line in the oracle file. Then resultsChecker.py compares the ExpectedOutcome line in the results file and Outcome line in the oracle file. If the values match then script will write a Results line in the results file saying “Pass”, otherwise the Results line will state “Fail.”
14) frameworkRunner.py will invoke reportMaker.py
15) reportMaker.py will read each TestCaseXXresults.txt file and into an HTML table it will write for each test case the values for the following components: Test Case Number, Component, Method, Requirement, Test Inputs, Recorded Outcome, Expected Outcome, and Result.
16) reportMaker.py will then include pertaint details such as the time and date of the tests, the number of test cases, the amount which passed, and links to the TestCaseXXresults.txt files for the failed tests.
17) Lastly, reportMaker.py will launch a web browser and display the testReportXX.html.

-------------------------------------------------------------------------------------------
Test Cases


A test case is a set of conditions to determine whether an application is fulfilling a requirement. Determination of the result of the test is through the comparison of the achieved result and the expected result. The expected result is defined by the test oracle. The results of each test case are record in a result file, and compiled into a report detailing the test cases ran, and their results.

-------------------------------------------------------------------------------------------
Test Case Template

Each testCaseXX.txt file follows the defined template. Failure to follow the template can result in inoperable tests, and/or erroneous test results.

TestCaseNumber:  00 // replace with appropriate test case number
Component: Classname.java
Method: The method signature of the method to be tested.
Requirement: Does the behavior and/or result of the above method perform as required?
TestInputs: Inputs to be used for testing the requirement
Oracle: TestCaseXXoracle.txt
Import: the package containing the class file to be tested.
Import: a new line for each additional import

Testing code
out.write("Outcome: " + outcome);
// change the variable ‘outcome’ to the value to be written
Listing X.X: testCaseTemplate.txt

-------------------------------------------------------------------------------------------
Test Case Example

The following is test case text file used to in unit testing the OpenMRS platform.

TestCaseNumber: 04
Component: Location.java
Method: Boolean isInHieracrchy(Location location, Location root)
Requirement: check to see if the location hierarchy is correctly implemented
TestInputs: Location objects: greatgrandparent, grandparent, parent, child
Oracle: TestCase04oracle.txt
Import: org.openmrs.*;

Location greatgrandparent = new Location();
Location grandparent = new Location();
Location parent = new Location();
Location child = new Location();

child.setParentLocation(parent);
parent.setParentLocation(grandparent);
grandparent.setParentLocation(greatgrandparent);
Boolean outcome = greatgrandparent.isInHierarchy(child, greatgrandparent);

out.write("Outcome: " + outcome);
Listing X.X: testCase04.txt

-------------------------------------------------------------------------------------------
Test Case Creation

In order to create the previous example, or a new test case the following steps must be taken.

1) Create a new text file called testCaseXX.txt, where XX is replaced by the appropriate test case number, for example testCase04.txt

2) In this new file copy the following information.

TestCaseNumber:
Component:
Method:
Requirement:
TestInputs:
Oracle:
Import:

out.write("Outcome: " + outcome);

3) Insert the test case number after ‘TestCaseNumber:’. The number must correspond with the number in the file name.

	TestCaseNumber: 04

4) Determine which component and which of its methods to test. Be sure to include the correct filename of the component, and the complete method signature to be tested.

Component: the class Location.java
Method: Boolean isInHieracrchy(Location location, Location root)

5) Determine what is the requirement/reason you are testing. For example: Boolean isInHieracrchy(Location location, Location root) determines if a Location object called location is within the parental hierarchy of the Location object root.

Requirement: check to see if the location hierarchy is created in the correct manner

6) Declare the inputs used to test the method.

TestInputs: Four Location objects called: greatgrandparent, grandparent, parent, child

7) Designate the filename of the corresponding oracle.

Oracle: TestCase04oracle.txt

8) Input the code necessary to test the requirement.

Location greatgrandparent = new Location();
Location grandparent = new Location();
Location parent = new Location();
Location child = new Location();

child.setParentLocation(parent);
parent.setParentLocation(grandparent);
grandparent.setParentLocation(greatgrandparent);
Boolean outcome = greatgrandparent.isInHierarchy(child, greatgrandparent);

The code listed above is inserted into the corresponding TestCaseXX.java file.  The default layout of a TestCaseXX.java file is shown next.

/*
TestCaseNumber: XX
Component: ClassName.java
Method: The method signature of the method to be tested.
Requirement: Does the behavior and/or result of the above method perform as required?
TestInputs: Inputs to be used for testing the requirement
Oracle: TestCaseXXoracle.txt
*/

import java.io.*;
import org.openmrs.*;

class TestCaseXX{
	public static void main(String args[]){
		try{
			FileWriter fstream;
			BufferedWriter out;
			
			out.close();
			fstream.close();

		}catch(Exception e){
			System.err.println("Error: " + e.getMessage());
		}
	}
}
Listing X.X: default template for each TestCaseXX.java file

The testing code from the text file is inserted in the corresponding TestCaseXX.java file. For example the testing code in Listing code will be written into TestCase04.java:

/*
TestCaseNumber: 04
Component: Location.java
Method: Booelean isInHieracrchy(Location location, Location root)
Requirement: check to see if the location hierarchy is correct
TestInputs: Four Location objects called: greatgrandparent, grandparent, parent, child
Oracle: TestCase04oracle.txt
*/

import java.io.*;
import org.openmrs.*;

class TestCase04{
	public static void main(String args[]){
		try{
			FileWriter fstream;
			BufferedWriter out;

			// FileWriter for testCase04.java
			fstream = new FileWriter("../temp/TestCase04results.txt", true);
			out = new BufferedWriter(fstream);

			Location greatgrandparent = new Location();
			Location grandparent = new Location();
			Location parent = new Location();
			Location child = new Location();
			child.setParentLocation(parent);
			parent.setParentLocation(grandparent);
			grandparent.setParentLocation(greatgrandparent);
			Boolean outcome = greatgrandparent.isInHierarchy(child, greatgrandparent);
			out.write("Outcome: " + outcome);
			out.close();
			fstream.close();

		}catch(Exception e){
			System.err.println("Error: " + e.getMessage());
		}
	}
}
Listing X.X: TestCase04.java. The bolded code is the testing code from testCase04.txt


9) Next ensure to write out the result of the test using the FileWriter variable out. The outcome will be then written to the corresponding TestCaseXXresults.txt file. The following is the default method of writing out the outcome.
	
out.write("Outcome: " + outcome);
// change the variable outcome to the value to be written

10) Lastly, place the completed testCaseXX.txt file into the testCases directory. 

-------------------------------------------------------------------------------------------
Test Oracles

A test oracle is used to determine whether a specified test has passed or failed. This determination is achieved by comparing the output(s) of a test, for a given input, to the predefined correct outputs located in the oracle. If the output of the test matches the output defined by the oracle, then the test has passed, otherwise the test has failed. This section details the test oracle template, and how to create an oracle.

-------------------------------------------------------------------------------------------
Oracle Template

Each TestCaseXXoracle.txt file must follow the defined template. Failure to conform to the oracle template can result in inaccurate test results.

TestCaseNumber: 00 // replace with appropriate test case number
Component: Classname.java
Method: The method signature of the method to be tested.
Requirement: Does the behavior and/or result of the above method perform as required?
TestInputs: Inputs to be used for testing the requirement
ExpectedOutcome: The expected outcome if the method was implemented correctly
Listing X.X: testCaseOracleTemplate.txt

-------------------------------------------------------------------------------------------
Oracle Example

The following example shows the layout of TestCase04oracle.txt file
	
	TestCaseNumber: 04
	Component: Location.java
	Method: Boolean isInHierarchy(Location location, Location root)
	Requirement: check to see if the Location hierarchy is correct
	TestInputs: Four Location objects: greatgrandparent, grandparent, parent, child
	ExpectedOutcome: true
Listing X.X: TestCase04oracle.txt

To create the TestCase04oracle.txt, or a new oracle file:

1) Create a blank text file. Name this file ‘TestCaseXXoracle.txt’, but replace the XX with the correct test case number.

2) Copy the oracle template into the new oracle file.

3)  Replace the template values with the values found within the testCaseXX.txt file. Only include the values for test case number, component, method, requirement, and test inputs.

4) Insert a new header ‘ExpectedOutcome:’.

5) Given the test inputs, determine what outcome should be produced by the test, and record this outcome after the ‘ExpectedOutcome:’ heading.

6) Save this file into the oracles directory.  